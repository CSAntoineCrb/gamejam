using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponAim
{
    NONE,
    SELF_AIM,
    AIM
}


public enum WeaponBulletType
{
    BULLET,
    ARROW,
    SPEAR,
    NONE
}
public enum WeaponFireType
{
    SINGLE,
    MULTIPLE
}

public class WeaponHandler : MonoBehaviour
{

    private Animator anim;

    public WeaponAim weapon_Aim;

    [SerializeField]
    private AudioSource shootSound;
    public WeaponFireType fireType;


    public WeaponBulletType bulletType;

   

    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    public void ShootAnimation()
    {
        anim.SetTrigger(AnimationTags.SHOOT_TRIGGER);
    }

    public void Aim(bool canAim)
    {
        anim.SetBool(AnimationTags.AIM_PARAMETER, canAim);
    }

   

    

    void Play_ShootSound()
    {
        shootSound.Play();
    }


   



} // class
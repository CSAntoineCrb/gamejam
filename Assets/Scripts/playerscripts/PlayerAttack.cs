using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    private WeaponManager weapon_Manager;



    private GameObject crosshair;

    private bool is_Aiming;
    private Animator zoomCameraAnim;
    [SerializeField]
    private GameObject spear_Prefab;

    [SerializeField]
    private Transform arrow_Bow_StartPosition;
    private Camera mainCam;


    void Awake()
    {

        weapon_Manager = GetComponent<WeaponManager>();
        mainCam = Camera.main;

    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        ZoomInAndOut();
        WeaponShoot();
    }



    void ZoomInAndOut()
    {



        if (weapon_Manager.GetCurrentSelectedWeapon().weapon_Aim == WeaponAim.SELF_AIM)
        {

            if (Input.GetMouseButtonDown(1))
            {

                weapon_Manager.GetCurrentSelectedWeapon().Aim(true);

                is_Aiming = true;

            }

            if (Input.GetMouseButtonUp(1))
            {

                weapon_Manager.GetCurrentSelectedWeapon().Aim(false);

                is_Aiming = false;

            }

        } // weapon self aim

    } // zoom in and out
    void WeaponShoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (is_Aiming)
            {

                weapon_Manager.GetCurrentSelectedWeapon().ShootAnimation();

                    // throw spear
                    ThrowArrowOrSpear();

                
            }
        }




    }
    void ThrowArrowOrSpear()
    {


            GameObject spear = Instantiate(spear_Prefab);
            spear.transform.position = arrow_Bow_StartPosition.position;
        spear.transform.rotation = arrow_Bow_StartPosition.rotation;

        spear.GetComponent<ArrowBowScript>().Launch(mainCam);

        }
    }

   // class